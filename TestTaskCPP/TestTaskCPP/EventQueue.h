#pragma once

#include <iostream>
#include <atomic>
#include <deque>
#include <mutex>
#include <map>
#include <functional>
#include <thread>
#include <chrono>

#include "Task.h"

class EventQueue
{	
	public:
		void run();
		void quit();
		Task setInterval(int interval, std::function<void()> callback);
		Task setTimeout(int timeout, std::function<void()> callback);

	private:
		struct DequeElement
		{
			bool IntervalOrTimeout;
			int time;
			std::function<void()> callback;
			std::shared_ptr<bool> stop;
			DequeElement() :
				IntervalOrTimeout(false),
				time(0),
				stop(NULL)
			{}
		};

		typedef std::shared_ptr<DequeElement> element;
		typedef std::deque<element>::iterator it_dequeElements;

		bool _quit = false;
		std::mutex _eventqueue_mutex;
		std::deque<element> _dequeElements;
};

