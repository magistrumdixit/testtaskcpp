#include "stdafx.h"
#include "EventQueue.h"


void EventQueue::run()
{
	std::cout << "Start run() " << std::endl;

	while (!_quit)
	{
		for (it_dequeElements i = _dequeElements.begin(); i != _dequeElements.end(); )
		{
			it_dequeElements it = i;
			element dequeElement = *i;

			if (!(dequeElement->IntervalOrTimeout))
			{
				std::this_thread::sleep_for(std::chrono::milliseconds(dequeElement->time));
				dequeElement->callback();
				(*dequeElement->stop) = true;
				std::cout << "Task Timeout complete" << std::endl;
			}
			else
			{
				while (!(*dequeElement->stop))
				{
					std::this_thread::sleep_for(std::chrono::milliseconds(dequeElement->time));
					dequeElement->callback();
					std::cout << "Task Interval complete" << std::endl;
				}
			}
			i = _dequeElements.erase(it);
		}
	}
	std::cout << "End run() " << std::endl;
}

void EventQueue::quit()
{
	_quit = true;
}

Task EventQueue::setInterval(int interval, std::function<void()> callback)
{
	auto _stop = std::make_shared<bool>(false);
	auto tmp = std::make_shared<DequeElement>();

	tmp->callback = callback;
	tmp->time = interval;
	tmp->IntervalOrTimeout = true;
	tmp->stop = _stop;

	_eventqueue_mutex.lock();
	_dequeElements.push_back(tmp);
	_eventqueue_mutex.unlock();

	return Task(_stop);
}

Task EventQueue::setTimeout(int timeout, std::function<void()> callback)
{
	auto _stop = std::make_shared<bool>(false);
	auto tmp = std::make_shared<DequeElement>();

	tmp->callback = callback;
	tmp->time = timeout;
	tmp->IntervalOrTimeout = false;
	tmp->stop = _stop;

	_eventqueue_mutex.lock();
	_dequeElements.push_back(tmp);
	_eventqueue_mutex.unlock();

	return Task(_stop);
}
