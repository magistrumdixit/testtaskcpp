// TestTaskCPP.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <ctime>
#include "EventQueue.h"
#include "Task.h"

#pragma warning(disable : 4996)

void Foo()
{
	std::cout << "Foo end " << std::endl;
}

void FooStop(Task taskFoo)
{
	std::this_thread::sleep_for(std::chrono::milliseconds(10000));
	if (!taskFoo.completed())
	{
		taskFoo.cancel();
	}
}

int main()
{
	EventQueue tmp;

	std::chrono::time_point<std::chrono::system_clock> start, end;

	std::function<void()> callback_setInterval = Foo;

	start = std::chrono::system_clock::now();
	std::time_t start_time = std::chrono::system_clock::to_time_t(start);
	std::cout << "Start: " << std::ctime(&start_time) << std::endl;

	std::thread thread_FooStop(FooStop, tmp.setInterval(1000, callback_setInterval));
	std::function<void()> callback_setTimeout = std::bind(&EventQueue::quit, &tmp);
	tmp.setTimeout(1000*60*5, callback_setTimeout);

	tmp.run();
	
	thread_FooStop.join();

	end = std::chrono::system_clock::now();
	std::time_t end_time = std::chrono::system_clock::to_time_t(end);
	std::cout << "End: " << std::ctime(&end_time) << std::endl;
	getchar();
    
	return 0;
}

