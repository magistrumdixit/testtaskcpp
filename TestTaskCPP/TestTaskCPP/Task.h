#pragma once

#include <thread>

class Task
{
	public:
		bool completed() const;
		void cancel();
		Task();
		Task(std::shared_ptr<bool> _stop);

	private:
		std::shared_ptr<bool> stop;
};

