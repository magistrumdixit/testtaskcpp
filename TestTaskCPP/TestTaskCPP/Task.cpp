#include "stdafx.h"
#include "Task.h"

Task::Task()
{
}

Task::Task(std::shared_ptr<bool> _stop)
{
	this->stop = _stop;
}

void Task::cancel()
{
	*stop = true;
}

bool Task::completed() const
{
	if (*stop)
		return true;
	else
		return false;
}
